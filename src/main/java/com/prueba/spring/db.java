package com.prueba.spring;

import com.mysql.cj.jdbc.MysqlDataSource;
//import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import com.mysql.jdbc.*;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.stream.Collector;
import java.io.*;
import java.util.*;
import com.prueba.spring.marcas;

public class db {
	public Connection cnn() {
    	String user = "root";
    	String pass = "master";
    	String data = "handson";
    	String serv = "localhost";
    	MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setUser(user);
        dataSource.setPassword(pass);
        dataSource.setDatabaseName(data);
        dataSource.setServerName(serv);
    	Connection conexion = null;
		try {
			conexion = dataSource.getConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return conexion;
    }
    //Marcas
    public List<marcas> getMarcas() {
    	int i = 0;
    	int size = 0;
    	ArrayList<marcas> marcas = new ArrayList<marcas>();
    	try {
	    	Statement st = this.cnn().createStatement();
	    	String sql = "SELECT "
	    					+ "id, "
	    					+ "Codigo, "
	    					+ "Nombre "
    					+ "FROM "
	    					+ "Marcas "
    					+ "ORDER BY Codigo";
	    	ResultSet rs = st.executeQuery(sql);
	    	rs.last();
	    	size = rs.getRow() - 1;
	    	rs.beforeFirst();
	    	if (size > 0) {
		    	while(rs.next()) {
		    		marcas.add(i, new marcas(rs.getObject("id").toString(), rs.getString("Codigo"), rs.getString("Nombre")));
		    		i += 1;
		    	}
	    	}
    	} catch (SQLException e) {

    	}
    	return marcas;
    }
}
