package com.prueba.spring;

public class marcas {
	private final String id;
	private final String codigo;
	private final String nombre;
	
	public marcas(String id, String codigo, String nombre) {
		this.id = id;
		this.codigo = codigo;
		this.nombre = nombre;
	}
	
	public String getId() {
		return id;
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	public String getNombre() {
		return nombre;
	}
}
