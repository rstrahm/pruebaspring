package com.prueba.spring;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class holaControler {

	private static final String template = "Hola, %s";
	private static AtomicLong counter = new AtomicLong(); 

	@RequestMapping("/prueba")
	public  hola stringPrueba(@RequestParam(value="nombre", defaultValue="mundo") String nombre) {
		return new hola(counter.incrementAndGet(), String.format(template, nombre));
	}
	
}