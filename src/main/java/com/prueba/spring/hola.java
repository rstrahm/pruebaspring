package com.prueba.spring;

public class hola {
	private final long id;
	private final String content;
	
	public hola(long id, String content) {
		this.id = id;
		this.content = content; 
	}
	
	public long getId() {
		return id;
	}
	
	public String getContent() {
		return content;
	}
}

